#!/usr/bin/env bash

# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

declare BUILD_TYPE="${1}"
declare BUILD_FOLDER="./build/${BUILD_TYPE}"

if [[ ! -d "${BUILD_FOLDER}" ]]; then
  echo -e "\033[0;31m'${BUILD_FOLDER}' is missing! Run configuration step first!\033[0m"
  exit 1
fi

cd "${BUILD_FOLDER}"

declare -i NUMBER_OF_PROCESSORS=$(($(ls /proc | grep ^[1-9] -c)/2))
export CTEST_OUTPUT_ON_FAILURE=1

echo -e "\033[1;33mctest -j${NUMBER_OF_PROCESSORS} --timeout 3\033[0m"
ctest -j${NUMBER_OF_PROCESSORS} --timeout 3
