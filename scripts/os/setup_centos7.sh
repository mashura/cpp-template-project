#!/usr/bin/env bash

# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

#------------------------------------------------------------------------------#
# Official Red Hat Enterprise Linux RPM software packages                      #
#------------------------------------------------------------------------------#

declare -a INSTALL_PKGS=(
  "cmake3"                        # 3.17.5
  "devtoolset-10-gcc"             # 10.2.1
  "devtoolset-10-gcc-c++"         # 10.2.1
  "devtoolset-10-gcc-gfortran"    # 10.2.1
  "devtoolset-10-gdb"             # 9.2
  "devtoolset-10-libasan-devel"   # 10.2.1 (The Address Sanitizer)
  "devtoolset-10-liblsan-devel"   # 10.2.1 (The Leak Sanitizer)
  "devtoolset-10-libtsan-devel"   # 10.2.1 (The Thread Sanitizer)
  "devtoolset-10-libubsan-devel"  # 10.2.1 (The Undefined Behavior Sanitizer)
  "devtoolset-10-make"            # 4.2.1
  "doxygen"                       # 1.8.5
  "git"                           # whatever
  "llvm-toolset-7.0-clang"        # 7.0.1
)

# Install packages

yum install -y centos-release-scl-rh epel-release
yum install -y --setopt=tsflags=nodocs ${INSTALL_PKGS[@]}
rpm -V ${INSTALL_PKGS[@]}
yum -y clean all --enablerepo='*'

#------------------------------------------------------------------------------#
# Packages Aliases                                                             #
#------------------------------------------------------------------------------#

#
# CMake
#

# See https://stackoverflow.com/questions/48831131

alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake 10 \
  --slave /usr/local/bin/ctest ctest /usr/bin/ctest \
  --slave /usr/local/bin/cpack cpack /usr/bin/cpack \
  --slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake \
  --family cmake

alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake3 20 \
  --slave /usr/local/bin/ctest ctest /usr/bin/ctest3 \
  --slave /usr/local/bin/cpack cpack /usr/bin/cpack3 \
  --slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake3 \
  --family cmake

#------------------------------------------------------------------------------#
# Python                                                                       #
#------------------------------------------------------------------------------#

pip3 install -r 'requirements.txt'

#------------------------------------------------------------------------------#
# Setup environment                                                            #
#------------------------------------------------------------------------------#

#
# Clang
#

# See https://gatowololo.github.io/blog/clangmissingheaders/
# See https://stackoverflow.com/questions/26333823

# rm -rf /usr/include/c++
# ln -s /opt/rh/devtoolset-10/root/usr/include/c++/10/ /usr/include/c++

# ln -s /opt/rh/gcc-toolset-10/root/lib/gcc/x86_64-redhat-linux/10/ /usr/lib/gcc/x86_64-redhat-linux/10

#
# Conan
#

# As we need to build some of Boost libraries we implicitly depends on b2 utility.
# The main problem here is that b2 is build wuith too modern glibc and libraries.
#
# This results in build crash:
#   b2: /lib64/libstdc++.so.6: version `GLIBCXX_3.4.21' not found (required by b2)
#
# I tried a lot of ways to solve this issue:
#   - see https://github.com/FoldingAtHome/fah-issues/issues/1147
#   - see https://stackoverflow.com/questions/19386651
#   - see https://stackoverflow.com/questions/29376130
#   - see https://stackoverflow.com/questions/42597701
#
# I even came up to this dirty hack that almost worked:
#
# function __hack {
#   libstdcpp_deb_link='http://ftp.de.debian.org/debian/pool/main/g/gcc-4.9/libstdc++6_4.9.2-10+deb8u1_amd64.deb'
#   libstdcpp_deb_dir='/tmp/libstdcpp_deb'
#   libstdcpp_deb_file="libstdc++6.deb"
#
#   mkdir "${libstdcpp_deb_dir}"
#   cd "${libstdcpp_deb_dir}"
#   curl -o "${libstdcpp_deb_file}" "${libstdcpp_deb_link}"
#   ar -x "${libstdcpp_deb_file}"
#   tar xvf 'data.tar.xz'
#
#   mkdir '/opt/libstdcpp'
#   cp './usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.20' '/opt/libstdcpp/libstdc++.so.6'
#   rm -rf "${libstdcpp_deb_dir}"
#
#   export LD_LIBRARY_PATH="/opt/libstdcpp:${LD_LIBRARY_PATH}"
# }
#
# But it has been dropped because it's error prone a lot.
# By some miracle, I found these two issues:
#   - https://github.com/conan-io/conan-center-index/issues/1116
#   - https://stackoverflow.com/questions/66487350
# that helped me to come up with a quite good workaround for this problem.

# Create and update Conan default profile

conan profile new default

conan profile update 'settings.arch=x86_64' default
conan profile update 'settings.arch_build=x86_64' default
conan profile update 'settings.os=CentOS' default
conan profile update 'settings.os_build=CentOS' default

# Create and update Conan settings

conan config init

sed -i -e 's/os_build: \[/os_build: \[CentOS, /g' "${HOME}/.conan/settings.yml"
sed -i -e $'s/Linux:/Linux:\\\n    CentOS:/g' "${HOME}/.conan/settings.yml"

#
# SCL (Software Collections)
#

# Make SCL avaible globaly in user environment.
# https://stackoverflow.com/questions/55901985
echo "source scl_source enable devtoolset-10" >> /etc/bashrc
echo "source scl_source enable llvm-toolset-7.0" >> /etc/bashrc
