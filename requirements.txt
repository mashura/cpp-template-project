# Build

# Force version because of https://github.com/conan-io/conan/issues/9695.
conan==1.40.3

# Documentation

breathe
exhale
furo
sphinx

# Linters

codespell
cmakelang
#cppcheck_codequality
#cppcheck_junit
gcovr
guardonce
licenseheaders
rstcheck
