/*
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_SHAPES_ACCESSOR_HPP
#define CTP_SHAPES_ACCESSOR_HPP

#include "shapes/shapes_factory.hpp"

#include <memory>

namespace Ctp {
namespace Shapes {

/**
 * @brief Main entry point for the @c Shapes module.
 *
 * The interface provides an ability to create objects of the @c Shapes module.
 */
class Accessor
    :   private boost::noncopyable
{

public:

    /**
     * @brief Default virtual desctuctor.
     */
    virtual ~Accessor () = default;

    /**
     * @brief Creates a @c ShapesFactory instance.
     *
     * @return @c ShapesFactory instance if it was created, @c nullptr otherwise.
     */
    [[nodiscard]]
    virtual std::unique_ptr< ShapesFactory > createFactory() const = 0;

    /**
     * @brief Creates an @c Accessor instance.
     *
     * @return @c Accessor instance if it was created, @c nullptr otherwise.
     */
    [[nodiscard]]
    static std::unique_ptr< Accessor > create ();

};

} // namespace Shapes
} // namespace Ctp

#endif // CTP_SHAPES_ACCESSOR_HPP

