/*
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_SHAPES_SRC_SHAPESFACTORYIMPL_HPP
#define CTP_SHAPES_SRC_SHAPESFACTORYIMPL_HPP

#include "shapes/shapes_factory.hpp"

namespace Ctp {
namespace Shapes {

class ShapesFactoryImpl final
    :   public ShapesFactory
{

public:

    std::unique_ptr< Rectangle > createRectangle (
        Common::Point _topLeftPoint,
        Common::Point _rightBottomPoint
    ) const override;

    std::unique_ptr< Rectangle > createRectangle (
        Common::Point _point,
        double _width,
        double _height
    ) const override;

};

} // namespace Shapes
} // namespace Ctp

#endif // CTP_SHAPES_SRC_SHAPESFACTORYIMPL_HPP
