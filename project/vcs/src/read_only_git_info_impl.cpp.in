/*
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/read_only_git_info_impl.hpp"

namespace Ctp {
namespace Vcs {

bool
ReadOnlyGitInfoImpl::isPopulated () const noexcept
{
    return @GIT_RETRIEVED_STATE@;
}

bool
ReadOnlyGitInfoImpl::hasUncommittedChanges () const noexcept
{
    return @GIT_IS_DIRTY@;
}

std::string_view
ReadOnlyGitInfoImpl::getAuthorName () const noexcept
{
    return "@GIT_AUTHOR_NAME@";
}

std::string_view
ReadOnlyGitInfoImpl::getAuthorEmail () const noexcept
{
    return "@GIT_AUTHOR_EMAIL@";
}

std::string_view
ReadOnlyGitInfoImpl::getCommitSha1 () const noexcept
{
    return "@GIT_HEAD_SHA1@";
}

std::string_view
ReadOnlyGitInfoImpl::getCommitDate () const noexcept
{
    return "@GIT_COMMIT_DATE_ISO8601@";
}

std::string_view
ReadOnlyGitInfoImpl::getCommitSubject () const noexcept
{
    return "@GIT_COMMIT_SUBJECT@";
}

std::string_view
ReadOnlyGitInfoImpl::getCommitBody () const noexcept
{
    return @GIT_COMMIT_BODY@;
}

std::string_view
ReadOnlyGitInfoImpl::getDescribe () const noexcept
{
    return "@GIT_DESCRIBE@";
}

} // namespace Vcs
} // namespace Ctp
