/*
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_COMMON_POINT_HPP
#define CTP_COMMON_POINT_HPP

#include "common/floating_point.hpp"

#include <ostream>

namespace Ctp {
namespace Common {

/**
 * @brief Representation of [Cartesian coordinate system][1] point.
 *
 * Provides an abstraction to a location in the space.
 *
 * [1]: <https://en.wikipedia.org/wiki/Cartesian_coordinate_system>
 */
struct Point final
{

public:

    /**
     * @brief Default constructor.
     */
    constexpr Point () noexcept
        :   m_x( 0.0 )
        ,   m_y( 0.0 )
    {}

    /**
     * @brief Constructs a point with the given @c _x and @c _y coordinates.
     */
    constexpr Point ( double _x, double _y ) noexcept
        :   m_x( _x )
        ,   m_y( _y )
    {}

    /**
     * @brief Get a distance from the point to to @c _rhs .
     *
     * @return Distance between points.
     */
    [[nodiscard]]
    constexpr double distanceTo ( Point const & _other ) const
    {
        double const diffX = _other.m_x - m_x;
        double const diffY = _other.m_y - m_y;

        return std::sqrt( ( diffX * diffX ) + ( diffY * diffY ) );
    }

    /**
     * @brief Checks whether the point is equal to @c _rhs.
     *
     * @return @c true if the point is equal to @c _rhs, @c false otherwise.
     */
    [[nodiscard]]
    constexpr bool operator == ( Point const & _other ) const
    {
        return isEqual( m_x, _other.m_x ) && isEqual( m_y, _other.m_y );
    }

    /**
     * @brief @c X coordinate of the point.
     */
    double const m_x;
    /**
     * @brief @c Y coordinate of the point.
     */
    double const m_y;

private:

    friend std::ostream & operator << ( std::ostream & _o, Point const & _point )
    {
        return _o << "Point{ " << _point.m_x << ", " << _point.m_y << " }";
    }

};

} // namespace Common
} // namespace Ctp

#endif // CTP_COMMON_POINT_HPP
