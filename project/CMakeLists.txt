# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(fts-src
  LANGUAGES
    CXX
  DESCRIPTION
    ""
)

#-----------------------------------------------------------------------------#
# Dependencies                                                                #
#-----------------------------------------------------------------------------#

find_package(docopt REQUIRED)

if(ENABLE_TESTING)
  find_package(Boost COMPONENTS REQUIRED unit_test_framework)
endif()

#-----------------------------------------------------------------------------#
# Coverage                                                                    #
#-----------------------------------------------------------------------------#

if(ENABLE_COVERAGE)
  setup_target_for_coverage_gcovr_html(
    NAME
      coverage
  )
endif()

#-----------------------------------------------------------------------------#
# Subdirectories                                                              #
#-----------------------------------------------------------------------------#

add_subdirectory(common)
add_subdirectory(exec)
add_subdirectory(shapes)
add_subdirectory(vcs)
